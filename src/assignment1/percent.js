 export let GetMarks = (percentage) =>{

    if(percentage>=90){

        return "Grade A";
    }
    else if(percentage>=80){
        
        return "Grade B";

    }
    else if(percentage>=70){

        
        return "Grade C";
    }
    else if(percentage>=60){

        
        return "Grade D";
    }
    else if(percentage>=40){

        
        return "Grade E";
    }
    else {

        
        return "Grade F";
    }
 }