export let takesInput = (admin) =>{
     
    let adminsuperAdmin = [ "admin", "superAdmin"];

    let _isAdminSuperAdmin = adminsuperAdmin.includes(admin)
    
    return _isAdminSuperAdmin;
}