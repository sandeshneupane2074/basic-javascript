export let firstCapital = (name) =>{
    let nameArr = name.split("");
    let _nameArr = nameArr.map( (value,i)=>{
        if(i===0){
            return value.toUpperCase();
        }
        else{
            return value.toLowerCase();
        }
    });

    let newsString= _nameArr.join("");
    return newsString;
};

