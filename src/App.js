
// import { address, age } from './abc';
import './App.css';
import { is18 } from './ass1';
import { isGreaterThan } from './ass2';
import { enterroom } from './ass3';
import { takesinputs } from './ass3/ass31';
import { sortDescending } from './ass3/ass32';
import { isChanged } from './ass3/ass33';
import { StringToArray } from './ass3/ass34';
import { ArrayToString } from './ass3/ass35';
import { arrayOfNumber } from './ass3/ass36';
import { pushvalue } from './ass3/ass37';
import { firstCapital } from './ass3/classass';

import { isEven } from './ass4';
import { averagge } from './ass5';
import { category } from './ass6';
import { tickets } from './ass7';
import { sumis } from './ass8';
import { reverseString } from './assignment1/Task2';
import { GetMarks } from './assignment1/percent';
import { IsInvalid } from './assignment1/takesinput';
import { filenames } from './mapass.js/js finalassignment/jsass1';
import { productsId } from './mapass.js/mapass1';
import { productsTitle } from './mapass.js/mapass2';
import { productsCategory } from './mapass.js/mapass3';
import { productsType } from './mapass.js/mapass4';
import { PriceMultiply } from './mapass.js/mapass5';
import { whosePrice3000 } from './mapass.js/mapass6';
import { newCategory } from './mapass.js/mapass7';
import { admin } from './string/Admin';
import { startsWithBearer } from './string/Bereaer';
import { takesInput } from './string/arraymethod/array1';
import { input } from './string/ass';
import { trimee } from './string/asss2';
import { lower } from './string/assstring1';
import { sentence } from './string/conver';
import { concerts } from './string/convert';



// import { average } from './average';
// import { sumfun } from './sumfunction';


function App() {

  // console.log("hello");
  // console.log("my name is sandesh Neupane");

  // console.log(1);
  // console.log(1+2);
  // console.log(true);
  // console.log (1+2+'a');
   

  // let name = "sandesh";
  // console.log(name);
  // name = "luffy";
  // console.log(name);

  // let a = 1;
  // let b = 2;
 
  // let c = a*b;
 
  // console.log(c);
  // console.log(age, address);
  // let a = false ;
  //  if(a){
  //   console.log("hello man");

  //  }
  //  else{
  //   console.log("i am else");
  //  }


//   let age = 30
//   if (age <= 17)
//   {
//  console.log("your ticket is free")
//   }
//   else if (age <= 25) {
//     console.log("ticket is 100")
//   } 
//   else if (age === 25) {
//     console.log("ticket is 150")
//   }
//   else if (age > 26) {
//     console.log("ticket is 200")
//   }
 

// let isEighteen = 18

// if (isEighteen ===18)
// {
// console.log("age is 18")
// }
// else if (isEighteen <= 25) {
//   console.log(" age is not 18")
// }

// let sum = function(a, b, c)
// {
//   console.log("function")
//   console.log(a)
//   console.log(b)
//   console.log(c)
// }
// sum(1, 2 , "sandesh")
// average ();
// sumfun(12, 2, 3)
// is18(18);
// isGreaterThan(19);
// enterroom (17);
// isEven(3)
// averagge(2 , 2 , 3)
// category(40)
// tickets(5)
// sumis(2 ,3)
// let _sumIs=sumis(1, 2)
// console.log(_sumIs);

// console.log("sandesh");
//  let sum = function(a , b) {
// let _sum = a+b;
// console.log("hari")
// return _sum;
// console.log("luffy");
// };

// console.log("neupane");
// let __sum= sum(1,2);
// console.log(__sum);

// let a = () =>{
//   return (1 , 2);
  // js returns only one values (1) , not (1,2) in this condition it returns 2 (last value)
// };
// let _a = a();
// console.log(_a);
// any function that returns another function is called hof(higher order function)

// string litreals => let fullName = `my name is ${} and surName is ${}`
// (``)=> backtick (we can call variable in ``)

// let name = "sandesh"
// let lastname = "Neupane"
// let address = "NewThimi,BKT"
// let details = `my name is ${name} & lastname is ${lastname} & address is ${address}`
// console.log(details);

// let defaultVal = (a,b=5) =>{
//   console.log(a);
//   console.log(b);
// };
// defaultVal(1,3)     (default parameter)
 
// key pionts =?("js is always execute a code from top to bottom approach")

// what is js?
// =>synchrounous (one task is done then another task is block)
// -=> single threaded language
//undefined means a variable is defined but not initilized
// == means it chekcs only value === checks values and datatypes
// let data = ["san", "man", 20 , false]
// console.log(data[1]);
// data[1]="luffy";
// console.log(data[1])

// let info = { name:"sandesh", surName:"Neupane", age:23, ismarried:false}
// console.log(info.name)
// info.name = "Luffy"
// console.log(info.name);
// console.log(info);
// delete info.surName
// console.log(info);
// info.roll = 425
// console.log(info);
// old key is duplicate above property is override by new key 

// let Marks = GetMarks(80);
// console.log(Marks);
// let myString = "Hello World!";
// console.log(myString.length)

// let String = "Hello World!";
// console.log(String.toUpperCase());
// console.log(String.toLowerCase());

// let myStrin = "Ram is a youtuber";
// console.log(myStrin.startsWith("Ram")); 
// console.log(myStrin.startsWith("ram"));

// let myStri = "John";
// console.log(myStri.endsWith("n")); 
// console.log(myStri.endsWith("N"));

// let strings = "   Hello Don  "
// console.log(strings.trim());
// console.log(strings.trimStart());
// console.log(strings.trimEnd()) 

// let sttring = " mernstack is taught by Nitan Thapa"
// console.log(sttring.includes("Nitan"));
// console.log(sttring.includes("nitan"));
// console.log(sttring.replaceAll("a" , "o"))



//  let _input = input("sandesh")
//  console.log(_input);
 
//  let _lower = lower("SANDESH")
//  console.log(_lower)

//  let _trimee = trimee("  Hello World  ")

//  let trimmedStart = _trimee.trimStart();
// let trimmedEnd = _trimee.trimEnd();

// console.log(trimmedStart);
// console.log(trimmedEnd); 

// let input1b = "Bearer horewa luffysan"
// let input2b = "Luffysan"

// console.log(startsWithBearer(input1b));
// console.log(startsWithBearer(input2b));

// let _sentence = sentence("mernstack is taught by Nitan Thapa")
// // let _sentence = "mernstack is taught by Nitan Thapa".replaceAll("")
// let __sentence = sentence(_sentence);

// console.log(__sentence);

// let _admin = admin("superadmin")
// console.log(_admin);

// let adminSuper = takesInput("luffy")
// console.log(adminSuper)

// conversion of array to string
// let list = ["a", "sandesh", "luffy", 1];

// let newString = list.join(" "); // for space
// console.log(newString);
// let newsString = list.join(""); //for non space
// console.log(newsString);

// // conversion of string to array

// let name = "acb deb ebfk";
// let newArray = name.split(" ");
// console.log(newArray)


// let _arrays = concerts("nitan")
// console.log(_arrays);

// push, pop , unshift, shift => change original array
// reverse and sort it gives new array change original array

// for capital words it comes first while sorting 
// let _IsInvalid = IsInvalid("active");
// console.log(_IsInvalid);


// let _reverseString = reverseString("hello")
// console.log(_reverseString)


// console.log(typeof "ram")
// VM515:1 string
// undefined
// console.log(1)
// VM565:1 1
// undefined
// console.log(typeof true)
// VM671:1 boolean
// type of operator 


// let fun = (arr1) =>{
//   if (arr1.length === 0){

//     return true
//   }
//   else{
//     return false
//   }

// }

// let _fun =fun([1,2,3])
// console.log(_fun)

// ["a", "b", "c"].forEach((value, i)=>{
//   console.log(value, i);
// });

// let arr1 = [1,2,3].map( (value , i) =>{

//   return value*2;
// })


// console.log(arr1)

// let _takesinputs = takesinputs([5, 4, 7, 6,2])
// console.log(_takesinputs);

// let _sortDescending = sortDescending([1, 2, 3, 4, 5])
// console.log(_sortDescending)


// let _isChanged = isChanged([1, 2, 3])
// console.log(_isChanged);

// let _StringToArray = StringToArray("my name is Sandesh Neupane")
// console.log(_StringToArray);

// let _ArrayToString = ArrayToString([1, 2, 3]);
// console.log(_ArrayToString);

// console.log(arrayOfNumber);

// let _pushvalue = pushvalue([1,2,3]);
// console.log(_pushvalue);

// let f1 = [1, 18,19,20].filter( (value, i) =>{

//   if (value>=18){
//     return true;
//   }
//   else{
//     return false;
//   }
// })

// console.log(f1)



// let f1 = [1, 18,19 ,20].find( (value,i) =>{
//   if(value>=18){
//     return true;
//   }
//   else{
//     return false;
//   }
// })
// console.log(f1);


// let f1 = [9, 19 ,18].every ( (value,i)=>{

//   if(value ===9){
//     return true;
//   }
//   else{
//     return false;
//   }
// })
// console.log(f1);

// in every array method return either true or false
// if all the element return true thenthe output will be else output will be false

// let issome = [1, 19, 17].some( (value,i)=>{

//   if(value>=18){
//     return true
//   }
//   else{
//     return false
//   }
// })
// console.log(issome);
// // in some array method return true in one output other wise false

// Q. make a arrow function that return true if one element of array is nitan

// let onelement = ["a", "b", "nitan"].some ( (value,i ) =>{
//   if(value==="nitan"){
//     return true
//   }
//   else {
//     return false
//   }
// })
// console.log(onelement);


// Q. make a arrow function that will return true if all  element of array is nitan
// let onelements = ["a", "b", "nitan"].every ( (value,i ) =>{
//   if(value==="nitan"){
//     return true
//   }
//   else {
//     return false
//   }
// })
// console.log(onelements);

//Q. input is ["nitan", "ram", "hari"]   filter those name which is nitan and ram (edited) 

// let isfilter = ["nitan", "ram", "hari"].filter ( (value,i) =>{

//   if(value==="nitan"|| value ==="ram"){
//     return true
//   }
//   else{
//     return false
//   }
// })
// console.log(isfilter);

//Q.make a arrow function  named firstLetterCapita that takes input as "nitan", it must return "Nitan"

// let _firstCapital =  firstCapital("Hari");
// console.log(_firstCapital);


// let obj1 = {
//   name:"sandesh",
//   age:23,
//   roll:1
// };
// let keysArray = Object.keys(obj1);
// let valuesArray = Object.values(obj1);

// console.log(keysArray);
// console.log(valuesArray);

// let _productsId = productsId()

// console.log(_productsId);

// console.log(productsId);

// console.log(productsTitle);

// console.log(productsCategory);

// console.log(productsType);
// console.log(PriceMultiply);

// let _whosePrice3000 = whosePrice3000.map( (value,i)=>{
//   return value.title;
  
// })
// console.log(_whosePrice3000);

// throw error
// let error = new Error("you have generated new eroor bakayaru");
// throw error

// try {
//   let error = new Error("error vateyo");
//   throw error;

//   console.log("i am try");
// } catch (error) {
  
//   console.log("i am catch");
  
//   console.log(error.message);
// }

// spread operator example
// let ar1 = [1,2,3]
// let ar2 = ["a","b","c"]
// let ar3 = [...ar1, ...ar2 , 5,6];

// console.log(ar3);

// for array it is used to open in wrapper
// to used spread operator in array we must keep its value inside array and to keep value in object we must keep it inside object so both are not possible together

// let info1 = {name:"sandesh", age:"23"}
// let info2 = {name:"sandy", age:"22"}
// let info3 = {...info1, ...info2};
// console.log(info3);

//set

// let s = new Set([1,2,3,1,2]);
// console.log(s);
// let s = [...new Set([1,1,2,3,4])]
// console.log(s);

// let s = [...new Set(newCategory)];
// console.log(s); 
 /// !!!!!!! mapass7.js problem solved.....

// let s = [...new Set(["a","b","a","d"])]
// console.log(s);
// let [a=7,b=5,c=6,d=10] =[1,2,3]
// console.log(d); Q. array destructing 

//object destructing

// let {age="22", name="sandy" , address="NewThimi"} = {name:"sandesh", age:23}
// console.log(age,name,address); it does not cares about order but cares about keys in object destructing

// alias => alias is used in object to change key :is used 
//for eg
// let {name, age} = {name:"sandesh", age:23}
// let {name:nam="ram", age:ag=40,address:adde="NewThimi"} = {name:'luffy',age:24}
// console.log(nam,ag,adde);
// console.log(name,age);
// default value is placed on last


// let mixfunction = (obj2,ar1) =>{

//   let arar = ["name", "address"];
//    let obj22 = {name:"sandesh", age:23, roll:1};
// }

//primitive data types stores value and it does not see whether it is copy or not
// non primitive checks value and memory allocation address
//in non primtive  it see whether variable is copy or not but if variable is  copy then it does not create memory for that variable but it shares memory

//non primitive pass by Reference
//primituve value pass by value

// set timeout

// setTimeout(()=>{
//   console.log("i am luffy");
// },1000)

// // set interval

// setInterval(()=>{
//   console.log("i am sandesh");
// },1000)

// setTimout is asynchronous function

// memory quee store values and after complete js c0de it will execute 
// asynchronous function is those which pushes it value to background for example browser it store values on memory queee

// let info={name:"nitan", age:"29"}
// let info1={name:"ram",age:"30"}
// let info2= {...info1,...info, age:"40",}
// console.log(info2);

// let in = [1,2,3].forEach( (value)=>{

  
// })

// reinitaialization is not possible in const but it is possible in let.

// block level scope => a variable will be known within its block only 
// if we call a variable then atfirst it search its own  block then focus parent block then grandparent blocks.

// variable will store either data or function

// arrow function vs noraml function
// normal function support this keyword
// for Eg:
/* let obj ={
  name:"sandesh",
  surname:"Neupane"
}
let  fullName: function() {
  retun `${this.name}, ${ this.surname}`
}
console.log(obj.name);
console.log(fullName()); */

/* Types of export:
export by name - Use curly brackets { }
export by default - do not use curly brackets
In a file, we cannot have more than 1 export by default but can have multiple export by name */


/* let name = "sandesh"
let age = 23
let c = name || age || false
let d = name ?? age ?? null
console.log(d); 
// || operator is used for intilaztion to achieve defalut property (all are falsi)

// nullish ?? null and undefined lai falsi manxa
console.log(c);
  */


// find the sum of each element
/* let a1 =  [1,2,3].reduce( (pre, cur)=>{
  return pre *cur
},1);
console.log(a1);  */


// console.log(filenames);


  // let date = new Date().toLocaleString();
  // console.log(date); 
 
//   let list = [1,2,3,4,5,6,7,8].flatMap( (value,i)=>{
//     if(value ===1 || value===3){
// return [];
//     }
//     else{
// return [ `a${value}`];
//     }

//   });
//   console.log(list); */

// let funct = (a, b, ...c) =>{

// }
// funct(1,2,3,4,5,6)
// console.log(funct);

/* let a = age;
let b = info;
let {age, ...info} = {name:"Sandesh",age:23,roll:425}
console.log(a,b); */// rest operator
// rest operation is used in received section


// let ar1 = [ {
//     name: "nitan",
//     gender: "male" }, {
//     name: "sita",
//     gender: "female"},{
//     name: "hari",
//     gender: "male"},{
//     name: "gita",
//     gender: "female"},{
//     name: "utshab",
//     gender: "other"}
// ];

// let groupGender = ar1.reduce((prev, cur) => {
//   if (!prev[cur.gender]) {
//     prev[cur.gender] = [];
//   }
//   prev[cur.gender].push(cur);
//   return prev;
// }, {});

// console.log(groupGender);

// let array1 = ['A','B','A', 'B','E'].reduce((prev,cur)=>{

//   return 
// },0)
let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "other",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "other",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "other",
    },
  },
];

let sortedarray =products.sort((a,b)=>{
 
return a.id-b.id;


})
console.log(sortedarray);

return (
    <div className="App">



    </div>
    // todo multiple comment press shift+alt+a
  );
}

export default App;
// 1+2=3;
// 1+2+"a"="3a"
// 1+2+"a"+3+1="3a31"

//sum(1,2) => use this syntax if the function does not contain return
// let a = sum(1,2) => use this syntax if the function does contain return